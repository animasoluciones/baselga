/**
 * Created by yusmel on 26/06/17.
 */
(function($) {
    var posUrbanismo=parseInt($("#urbanismo").offset().top);
    var posInmobiliario=parseInt($("#inmobiliario").offset().top);
    var posCatastral=parseInt($("#catastral").offset().top);
    var posExpropiacion=parseInt($("#expropiacion").offset().top)
    var posContact=parseInt($("#contacto").offset().top);
    var posScroll=$(document).scrollTop();
   






    var mac=window.navigator.platform;
    var os = mac.substring(0, 3);

    if($(window).height()<=604){
        $('.texto p').css('padding','0');
        $('section img').width('50%');
    }

    console.log($(window).height() +" "+$(window).width());
    
    

    /*first thing--> event rezise*/

    $( window ).resize(function() {
      $('section').css('height','auto');
      if($(window).width()>=1443){
          $('section').height($( window ).height());
      }

        if($(window).width()<634){
            $('section img').width(window.innerHeight);
        }



    });

    
    /*This section is for mac only*/

    // $('section').height($( window ).height()).addClass('bg');

     if(os=="Mac"){
        $(".menu li").each(function(){
            $(this).css('height','3rem');
            $(this).children().css({'font-size':'2.3rem','top':'-11px'});
        });
     }

    


    if(posScroll==0){
        $(".downd li:last-child").hide();
    }



    $(document).bind( "scroll", function(event) {

      if($(document).scrollTop()>posScroll){
            posScroll=$(document).scrollTop();
           
           if(posScroll>0){
                $(".downd li:last-child").show();
           }


           if(posScroll>=$('section').height()*4 && os!="Mac"){
                $(".downd li:first-child").hide();
                 
           }else if(os=="Mac" && posScroll>=2800){
                $(".downd li:first-child").hide();
           }


        }else {
            posScroll=$(document).scrollTop();
            
            if(posScroll==0){
                $(".downd li:last-child").hide();
           }
           if(posScroll-110<posContact){
                $(".downd li:first-child").show();
           }

        }
    });



    /*$('section').hover(
        function () {

            $($(".sect")[parseInt($(this).attr('data-id'))]).addClass('sect-active');

        },
        function () {
            $($(".sect")).each(
                function(){
                    $(this).removeClass('sect-active');
                }
            );
        }
    );*/
    
    $('section').each(function (index) {
        $(this).mouseover(function () {
            $('.sect-active').removeClass('sect-active');
            $($(".sect")[parseInt($(this).attr('data-id'))]).addClass('sect-active');
        });
    });

    /*$('.footer').mouseover(function () {
        $('.sect-active').removeClass('sect-active');
        $('.contacto').addClass('sect-active');
    });*/










    $('a[href*="#"]')
    //  elimina los links que actualmente no van a ningun sitio(o sea no los tiene en cuenta)
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function(event) {
            // On-page links
            if ( location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                // Figure out element to scroll to
                var target = $(this.hash);
                if($(this).attr('data-next')=='0'){
                    $('section').mouseover(function(){
                        console.log("si aqui");
                    });
                }


                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                // Does a scroll target exist?
                if (target.length) {
                    // Only prevent default if animation is actually gonna happen
                    event.preventDefault();
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000, function() {
                        // Callback after animation
                        // Must change focus!
                        var $target = $(target);
                        $target.focus();
                        if ($target.is(":focus")) { // Checking if the target was focused
                            return false;
                        } else {
                            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                            $target.focus(); // Set focus again
                        };
                    });
                }
            }
        });



    $('.downd li img').click(function (event) {
        event.preventDefault();

        if($(this).attr('data-action')=='down'){
            target=$($('.sect-active')[0].hash).next();
            $('.sect-active').removeClass('sect-active').parent().next().find('a').addClass('sect-active');

        }else if($(this).attr('data-action')=='up'){
            target=$($('.sect-active')[0].hash).prev();
            $('.sect-active').removeClass('sect-active').parent().prev().find('a').addClass('sect-active');
        }


        $('html, body').animate({
            scrollTop: target.offset().top
        }, 1000, function() {
            // Callback after animation
            // Must change focus!
            var $target = $(target);
            $target.focus();
            if ($target.is(":focus")) { // Checking if the target was focused
                return false;
            } else {
                $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                $target.focus(); // Set focus again
            };
        });

    });

    /*Añadiendo la clase active en cada uno de los links*/
    $(".sect").click(function(){
        $(".sect").each(
            function(){
                $(this).removeClass('sect-active');
            }
        );
        $(this).addClass('sect-active');
    });

    function upDown(elemento) {
        $('html, body').animate({
            scrollTop: $(elemento).offset().top-1
        }, 1000);

        //$( document).unbind( "scroll" );
    }


    function nav(){
            var nVer = navigator.appVersion;
            var nAgt = navigator.userAgent;
            var browserName  = navigator.appName;
            var fullVersion  = ''+parseFloat(navigator.appVersion); 
            var majorVersion = parseInt(navigator.appVersion,10);
            var nameOffset,verOffset,ix;

            // In Opera, the true version is after "Opera" or after "Version"
            if ((verOffset=nAgt.indexOf("Opera"))!=-1) {
               browserName = "Opera";
               fullVersion = nAgt.substring(verOffset+6);
               if ((verOffset=nAgt.indexOf("Version"))!=-1) 
                 fullVersion = nAgt.substring(verOffset+8);
            }
            // In MSIE, the true version is after "MSIE" in userAgent
            else if ((verOffset=nAgt.indexOf("MSIE"))!=-1) {
               browserName = "Microsoft Internet Explorer";
               fullVersion = nAgt.substring(verOffset+5);
            }
            // In Chrome, the true version is after "Chrome" 
            else if ((verOffset=nAgt.indexOf("Chrome"))!=-1) {
               browserName = "Chrome";
               fullVersion = nAgt.substring(verOffset+7);
            }
            // In Safari, the true version is after "Safari" or after "Version" 
            else if ((verOffset=nAgt.indexOf("Safari"))!=-1) {
               browserName = "Safari";
               fullVersion = nAgt.substring(verOffset+7);
               if ((verOffset=nAgt.indexOf("Version"))!=-1) 
                 fullVersion = nAgt.substring(verOffset+8);
            }
            // In Firefox, the true version is after "Firefox" 
            else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) {
                browserName = "Firefox";
                fullVersion = nAgt.substring(verOffset+8);
            }
            // In most other browsers, "name/version" is at the end of userAgent 
            else if ( (nameOffset=nAgt.lastIndexOf(' ')+1) < (verOffset=nAgt.lastIndexOf('/')) ) {
                browserName = nAgt.substring(nameOffset,verOffset);
                fullVersion = nAgt.substring(verOffset+1);
                if (browserName.toLowerCase()==browserName.toUpperCase()) {
                   browserName = navigator.appName;
                }
            }
            // trim the fullVersion string at semicolon/space if present
            if ((ix=fullVersion.indexOf(";"))!=-1)
                fullVersion=fullVersion.substring(0,ix);
            if ((ix=fullVersion.indexOf(" "))!=-1)
                fullVersion=fullVersion.substring(0,ix);

            majorVersion = parseInt(''+fullVersion,10);
            if (isNaN(majorVersion)) {
                fullVersion  = ''+parseFloat(navigator.appVersion); 
                majorVersion = parseInt(navigator.appVersion,10);
            }

            return browserName;

        }





})(jQuery);